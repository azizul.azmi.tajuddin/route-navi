import React from 'react';
import { Route, IndexRoute } from 'react-router';

//list our page
import App from './components/app';
import DashBoard from './components/dashboard';
import Login from './components/login';
import Content from './components/contents';

//declare our path
export default (
  <Route path='/' component={App}>
    <IndexRoute component={DashBoard} />
    <Route path='dashboard' component={DashBoard} />
    <Route path='login' component={Login} />
    <Route path='content' component={Content} />
  </Route>
);