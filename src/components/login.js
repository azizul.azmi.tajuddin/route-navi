//=================
// Import the dependencies that this component will need
// to function properly
//=================
import React, { Component } from "react";
import { Link } from 'react-router';

//=================
// This component will eventually act like our login Form
// but for now it's an alternate route we can test out
// to make sure our router is functioning.
//=================
export default class Login extends Component {
  render() {
    return (
      <div>
        <p>This is the login page</p>
        <Link to="content"><button>Go To Content</button></Link>
      </div>
    );
  }
}