//=================
// Import the dependencies that this component will need
// to function properly
//=================
import React, { Component } from "react";
import { Link } from 'react-router';

//=================
// This is the component we'll be importing as the index route of our
// router.
//=================
export default class Dashboard extends Component {
  render() {
    return (
      <div>
        <p>This is the dashboard page</p>
        <Link to="login"><button>Go To Login</button></Link>
      </div>
    );
  }
}