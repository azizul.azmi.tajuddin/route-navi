//=================
// Import the dependencies that this component will need
// to function properly
//=================
import React, { Component } from "react";
import { Link } from 'react-router';

//=================
// This component will eventually act like our login Form
// but for now it's an alternate route we can test out
// to make sure our router is functioning.
//=================
export default class Contents extends Component {
  render() {
    return (
      <div>
        <p>This is the Content page</p>
        <Link to="dashboard"><button>Go To Dashboard</button></Link>
      </div>
    );
  }
}