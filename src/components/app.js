import React, { Component } from "react";

import NAVIGATION from './navigation';

export default class App extends Component {
  render() {
    return (
      <div>
          	<NAVIGATION />
        	{this.props.children}
      </div>
    );
  }
}